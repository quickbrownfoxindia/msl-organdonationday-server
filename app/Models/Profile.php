<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Profile extends Model
{

const CREATED_AT = 'created';
const UPDATED_AT = 'updated';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    public function user()   {
        return $this->BelongsTo(User::class);
    }


}
