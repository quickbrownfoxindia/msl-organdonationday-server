<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Donee extends Model
{
    protected $guarded = ['id'];
    const CREATED_AT = 'created';
    const UPDATED_AT = 'updated';

}
