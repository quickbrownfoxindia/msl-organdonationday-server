<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ProfileNew extends Model
{

const CREATED_AT = 'created';
const UPDATED_AT = 'updated';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    protected $table = 'profiles_new';


    public function user()   {
        return $this->BelongsTo(UserNew::class, 'user_id');
    }


}
