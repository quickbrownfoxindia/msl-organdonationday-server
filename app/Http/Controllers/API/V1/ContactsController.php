<?php
namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;
use Mail;

class ContactsController extends Controller  {

	public function contact(Request $request)
	{
        $data = $request->json()->all();
		try {
			Mail::send('emails.contact', ['name' => $data['name'],'email' => $data['email'], 'desc' => $data['message']],
				function ($message)  use($data){
					$message->from('info@organdonationday.in');
					$message->to('info@organdonationday.in');
					$message->subject('Thank You For Contacting');
				}
			);
		}
		catch(\Exception $e)	{
			return response()->json(['status' => 0, 'message' => 'Mail Not Sent', 'error' => $e->getMessage()], 500);

		}
		
		return response()->json(['status' => 1, 'message' => 'Mail Sent'], 200);
	}

}