<?php
namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Donee;
use App\Models\User;

class SearchController extends Controller
{
    public function searchUser(Request $request)    {
        $data   =   $request->json()->all();
        $email  =   $data['email'];
        $type   =   $data['donor'];
        //$date   =   $data['dob']['year'].'-'.sprintf('%02d',$data['dob']['month']).'-'.sprintf('%02d',$data['dob']['day']);
        //$dob    =	date('d/m/Y',strtotime($date));
        $dob    =   $data['dob'];
        if($type == 'donor')    {
            \DB::enableQueryLog();
            $user       = User::where('email', $email)->where('is_deleted', 0)->whereHas('profile', function($query) use($dob){
                $query->where('dob', $dob);
            })->get();
            /*$profile =   Profile::where('dob',$dob)->whereHas('user', function($query) use($email){
                $query->where('email',$email);
            })->get();*/

            if($user->count() == 0)   {
                return response()->json(['status' => 2, 'message' => 'Profile Not Found', 'data' => \DB::getQueryLog(), 'date' => $date], 500);
            }
            return response()->json(['status' => 1, 'message' => 'Result Found', 'profile' => $user[0]->profile], 200);
        }
        else {
            $donee  =   Donee::where('email', $email)->where('dob', $dob)->where('disabled', 0)->first();
            if(is_null($donee))   {
                return response()->json(['status' => 4, 'message' => 'Data Not Found'], 500);
            }
            return response()->json(['status' => 3, 'message' => 'Result Found', 'donee' => $donee], 200);
        }
       
    }

}
