<?php
namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Profile;
use App\Models\Donee;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;
use Mail;
use Carbon\Carbon;

class CronController extends Controller  {
    
    function sendReminderEmail() {

        User::where('created', '>=', Carbon::now()->subDay())->chunk(100, function($users) {

            foreach($users as $user) {

                    if(!$user->profile) {
                        echo $user->email . PHP_EOL;


                        Mail::send('emails.reminder', [],
                            function ($message) {
                                $message->from('info@organdonationday.in');
                                $message->to('hello@neerajkumar.name');
                                $message->subject('Please complete registration');
                            }
                        );
                        
                        
                        
                    } else {
                        ;
                    }

            }

        });
    }
}
