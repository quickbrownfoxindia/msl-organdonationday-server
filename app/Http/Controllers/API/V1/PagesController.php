<?php
namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\Models\Page;
use Illuminate\Http\Request;

class PagesController extends Controller  {

    public function showPage(Request $request) {
        $data = $request->json()->all();

        $page = Page::where('slug', $data['slug'])->first();

        if(is_null($page)) {
            return response()->json(['status' => 0, 'message' => 'page not found'], 500);
        }

        return response()->json(['status' => 1, 'message' => 'Data Successfully Sent', 'page' => $page], 200);
    }

}
