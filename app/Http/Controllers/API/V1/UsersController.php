<?php
namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Profile;
use App\Models\Donee;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;
use Mail;

class UsersController extends Controller  {

	public function addUser(Request $request)
	{
		$data['name'] = '';
        $data = $request->json()->all();
        if (filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
            $user = User::where('email', $data['email'])->first();
            if(is_null($user)) {

                $user = new User;
                $user->email = $data['email'];
                $user->origin = $data['source'];

                if($data['source'] === 'google') {
                    $user->name = $data['name'];
                    $user->google_uid = $data['uid'];
                }
                elseif($data['source'] === 'facebook')  {
                    $user->name = $data['name'];
                    $user->facebook_uid = $data['uid'];
                }
                elseif($data['source'] === 'linkedin') {
                    $user->name = $data['name'];
                    $user->linkin_uid = $data['uid'];
                }
                else {
                    $user->name = '';
                }

				$user->is_deleted = 0;

                try {
                    $user->save();
                }
                catch(\Exception $e)    {
                    return response()->json(['status' => 0, 'message' => $e->getMessage()], 500);
                }
				
				return response()->json(['status' => 1, 'message' => 'User Added Successfully', 'user_id'   =>  $user->id], 200);
            }
            else {

				$user->is_deleted = 0;

                try {
                    $user->save();
                }
                catch(\Exception $e)    {
                    return response()->json(['status' => 0, 'message' => $e->getMessage()], 500);
                }

                $profile = Profile::where('user_id', $user->id)->first();
                if(is_null($profile)) {
                     return response()->json(['status' => 2, 'message' => 'User Already Exists', 'user_id'   =>  $user->id], 200);
                }
                return response()->json(['status' => 3, 'message' => 'User Already Exists', 'user_id'   =>  $user->id, 'profile' => $profile], 200);
            }

        }
        else {
            return response()->json(['status' => 0, 'message' => 'Email Not Valid'], 500);
        }
	}

    public function addProfile(Request $request)    {
        $data = $request->json()->all();
        $user = User::findOrFail($data['user_id']);
        if(is_null($user)) {
            return response()->json(['status' => 0, 'message' => 'User Not Found'], 500);
        }
        $profile = Profile::where('user_id', $data['user_id'])->first();
        if(is_null($profile)) {
            $profile =  new Profile;
			$profile->user_id = $data['user_id'];
        }
        if($data['name']	!=	null)	{
			$profile->name      = $data['name'];
		}
		else {
			$profile->name      = '';
		}
		if(isset($data['dob']))	{
			if(!isset($data['dob']['year'])) {
				$date = date('Y-m-d', strtotime(str_replace('/', '-', $data['dob'])));
				$profile->dob       =	date('d/m/Y',strtotime($date));
			}
			else {
				$date   =   $data['dob']['year'].'-'.sprintf('%02d',$data['dob']['month']).'-'.sprintf('%02d',$data['dob']['day']);
				$profile->dob       =	date('d/m/Y',strtotime($date));
			}
			
		}
		else {
				//$profile->dob       = '';
		}
		if($data['gender'] != null)	{
			$profile->gender    = $data['gender'];
		}
		else {
			$profile->gender    ='';
		}
		if($data['bgroup'] != null)	{
			$profile->bgroup    = $data['bgroup'];
		}
		else {
			$profile->bgroup    = '';
		}
		if($data['fatherName'] != null)	{
			$profile->fname     = $data['fatherName'];
		}
		else {
			$profile->fname     = '';
		}
		if($data['contactNumber'] != null)	{
			$profile->number    = $data['contactNumber'];
		}
		else {
				$profile->number    = '';
		}
		if($data['address'] != null)	{
			$profile->address   = $data['address'];
		}
		else {
			$profile->address   = '';
		}
		if($data['city'] != null)	{
			$profile->city      = $data['city'];
		}
		else {
			$profile->city      = '';
		}
		if($data['state'] != null)	{
			$profile->state     = $data['state'];
		}
		else {
			$profile->state      = '';
		}
		if($data['pincode'] != null)	{
			$profile->pincode   = $data['pincode'];
		}
		else {
			$profile->pincode      = '';
		}
		if($data['emergencyContactName'] != null)	{
			$profile->ename    = $data['emergencyContactName'];

		}
		else {
			$profile->ename    = '';
		}
		if($data['emergencyContactNumber'] != null)	{
			$profile->enumber   = $data['emergencyContactNumber'];

		}
		else {
			$profile->enumber    = '';
		}
		if($data['organs'] != null)	{
			$profile->organs    = json_encode($data['organs']);
		}
		else {
			$profile->organs	=	'{}';
		}

        try {
            $profile->save();

            try {
            	if(!$profile->is_email_sent) {
				Mail::send('emails.register', ['name' => $profile->name, 'email' => $user->email],
					function ($message)  use($data, $user){
						$message->from('info@organdonationday.in');
						$message->to($user->email);
						$message->subject('Thank you for registration');
					}
				);
					$profile->is_email_sent = 1;
					$profile->save();
					return response()->json(['status' => 1, 'message' => 'Email sent and Saved Successfully '. $data['user_id'] , 'profile'   =>  $profile], 200);
				} else {
					return response()->json(['status' => 1, 'message' => 'Email not sent as it was sent earlier and Saved Successfully '. $data['user_id'] , 'profile'   =>  $profile], 200);
				}
            } catch(\Exception $e)    {
	            return response()->json(['status' => 2, 'message' => 'Email not sent error '. $data['user_id'] , 'error'   =>  $e->getMessage()], 200);
	        }
        	
			
        }
        catch(\Exception $e)    {
            return response()->json(['status' => 2, 'message' => 'Unable To Save Profile', 'error'   =>  $e->getMessage()], 500);
        }
        return response()->json(['status' => 1, 'message' => 'Profile Saved Successfully', 'profile'   =>  $profile], 200);
    }

	public function deleteUser(Request $request)	{
		$data = $request->json()->all();

		$user	=	User::where('id', $data['user_id'])->first();

		if(is_null($user))	{
			return response()->json(['status' => 0, 'message' => 'User Not Found'], 500);
		}
		$user->is_deleted = 1;

		try {
			$user->save();
		}
		catch(\Exception $e)	{
			return response()->json(['status' => 2, 'message' => 'Unable To Delete', 'error'   =>  $e], 500);
		}
		return response()->json(['status' => 1, 'message' => 'User Deleted'], 200);
	}

	public function addDonee(Request $request)    {
        $data	= $request->json()->all();
        $donee 	= Donee::where('email', $data['email'])->first();
        if(is_null($donee)) {
            $donee =  new Donee;
			$donee->user_id = 0;
        }
        if($data['name']	!=	null)	{
			$donee->name      = $data['name'];
		}
		else {
			$donee->name      = '';
		}
		if($data['email']	!=	null)	{
			$donee->email      = $data['email'];
		}
		else {
			$donee->email      = '';
		}
		if($data['contact'] != null)	{
			$donee->number    = $data['contact'];
		}
		else {
			$donee->number    = '';
		}
		if($data['gender'] != null)	{
			$donee->gender    = $data['gender'];
		}
		else {
			$donee->gender    ='';
		}
		if($data['bgroup'] != null)	{
			$donee->bgroup    = $data['bgroup'];
		}
		else {
			$donee->bgroup    = '';
		}
		if(isset($data['dob']))	{
			if(!isset($data['dob']['year'])) {
				$date = date('Y-m-d', strtotime(str_replace('/', '-', $data['dob'])));
				$donee->dob       =	date('d/m/Y',strtotime($date));
			}
			else {
				$date   =   $data['dob']['year'].'-'.sprintf('%02d',$data['dob']['month']).'-'.sprintf('%02d',$data['dob']['day']);
				$donee->dob       =	date('d/m/Y',strtotime($date));
			}
		}
		else {
				//$donee->dob       = '';
		}
		if($data['address'] != null)	{
			$donee->address   = $data['address'];
		}
		else {
			$donee->address   = '';
		}
		if($data['city'] != null)	{
			$donee->city      = $data['city'];
		}
		else {
			$donee->city      = '';
		}
		if($data['state'] != null)	{
			$donee->state     = $data['state'];
		}
		else {
			$donee->state      = '';
		}
		if($data['pincode'] != null)	{
			$donee->pincode   = $data['pincode'];
		}
		else {
			$donee->pincode      = '';
		}
		if($data['emergencyContactName'] != null)	{
			$donee->ename    = $data['emergencyContactName'];
		}
		else {
			$donee->ename    = '';
		}
		if($data['emergencyContactNumber'] != null)	{
			$donee->enumber   = $data['emergencyContactNumber'];
		}
		else {
			$donee->enumber    = '';
		}
		if($data['hosp'] != null)	{
			$donee->hosp   = $data['hosp'];
		}
		else {
			$donee->hosp    = '';
		}
		if($data['hosp_add'] != null)	{
			$donee->hosp_add   = $data['hosp_add'];
		}
		else {
			$donee->hosp_add    = '';
		}
		if($data['organs'] != null)	{
			$donee->organs    = json_encode($data['organs']);
		}
		else {
			$donee->organs	=	'{}';
		}
		$donee->disabled = 0;
        try {
            $donee->save();
        }
        catch(\Exception $e)    {
            return response()->json(['status' => 2, 'message' => 'Unable To Save Donee', 'error'   =>  $e->getMessage()], 500);
        }
        return response()->json(['status' => 1, 'message' => 'Donee Saved Successfully', 'profile'   =>  $donee], 200);
    }

	public function deleteDonee(Request $request)	{
		$data = $request->json()->all();

		$donee	=	Donee::where('id', $data['id'])->first();

		if(is_null($donee))	{
			return response()->json(['status' => 0, 'message' => 'Donee Not Found'], 500);
		}
		$donee->disabled = 1;

		try {
			$donee->save();
		}
		catch(\Exception $e)	{
			return response()->json(['status' => 2, 'message' => 'Unable To Delete', 'error'   =>  $e], 500);
		}
		return response()->json(['status' => 1, 'message' => 'User Deleted'], 200);
	}

	public function getUserDetails($id) {
		$user = User::where('id', $id)->with('profile')->first();

		if(is_null($user)) {
			return response()->json(['status' => 0, 'message' => 'Unable to find user'], 500);
		}
		if(is_null($user->profile)) {
			return response()->json(['status' => 0, 'message' => 'Profile Not Completed'], 500);
		}
		$data['name'] 	= $user->profile->name;
		
		$data['age'] 	= date_diff(date_create(date('Y-m-d', strtotime(str_replace('/', '-', $user->profile->dob)))), date_create(date('Y-m-d')))->y;
		//$data['age'] 	= date_diff(date_create(date('d/m/Y/', strtotime(str_replace('/', '-', $user->profile->dob)), date_create(date('d/m/Y')))->y;
		
		if($user->profile->bgroup == 'abplus')	{
			$data['bgroup'] = 'AB+ve';
		}
		elseif($user->profile->bgroup == 'abminus')	{
			$data['bgroup'] = 'AB-ve';
		}
		elseif($user->profile->bgroup == 'aplus')	{
			$data['bgroup'] = 'A+ve';
		}
		elseif($user->profile->bgroup == 'aminus')	{
			$data['bgroup'] = 'A-ve';
		}
		elseif($user->profile->bgroup == 'bplus')	{
			$data['bgroup'] = 'B+ve';
		}
		elseif($user->profile->bgroup == 'bminus')	{
			$data['bgroup'] = 'B-ve';
		}
		elseif($user->profile->bgroup == 'oplus')	{
			$data['bgroup'] = 'O+ve';
		}
		elseif($user->profile->bgroup == 'ominus')	{
			$data['bgroup'] = 'O-ve';
		}
		else {
			$data['bgroup'] = '';
		}

		$data['organs'] = $user->profile->organs;
		$data['emergency_contact'] = $user->profile->ename;
		$data['emergency_phone_number'] = $user->profile->enumber;


		return response()->json(['status' => 1, 'message' => 'User Found Successfully', 'user' => json_encode($data)], 200);

	}

	public function getDonorCount() {
		$users = \DB::table('users')->count();
		return response()->json(['status' => 1, 'message' => 'Count sent', 'cnt' => $users], 200);
	}
}
