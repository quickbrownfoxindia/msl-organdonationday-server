<?php
/**
 * Created by PhpStorm.
 * User: avinashmishra
 * Date: 04/05/17
 * Time: 2:37 PM
 */
namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class JsonRequestMiddleware {

	public function handle(Request $request, Closure $closure)  {

		if(in_array($request->method(), ['POST', 'PUT', 'PATCH']) && $request->isJson())    {
			$data   =   $request->json()->all();
			$request->request->replace(is_array($data) ? $data : []);
		}

		return $closure($request);

	}

}