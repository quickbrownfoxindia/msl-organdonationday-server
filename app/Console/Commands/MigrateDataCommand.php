<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use \App\Models\UserNew;
use \App\Models\DonorNew;
use \App\Models\ProfileNew;
use \App\Models\Profile;
use \App\Models\User;
use \App\Models\Donor;
use App\Models\UserOld;
use App\Models\UserOldOld;
use Log;

class MigrateDataCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'odd:migrate:data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migrate data from old structure to new';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle() {

        
        Donor::chunk(100, function($oldUsers) {

            foreach($oldUsers as $oldUser) {
                
                /* pick one user
                * check it's existance in donors_new table
                * if found, check modified date
                * if modified date of picked user is > donors_new user, then update the information
                * if not found, create a new user
                *
                */

                echo "Importing user #".$oldUser->id.PHP_EOL;

                
                $transformedData = ($this->getTransformedUser($oldUser));
                
                // dd($transformedData);
                echo "==========".PHP_EOL;

                if(DonorNew::where('email', $oldUser->email)->exists()) {

                    echo "user exists for email (".$oldUser->email.")" . PHP_EOL;
                    $newUser = DonorNew::where('email', $oldUser->email)->first();

                    if(\Carbon\Carbon::parse($oldUser->modified)->greaterThan($newUser->modified)) {

                        echo "Old user has latest information" . PHP_EOL;
                        echo $oldUser->modified . " " . $newUser->modified . PHP_EOL;
                        $newUser->update($transformedData);
                        echo "Updated new user with old data" . PHP_EOL;

                    } else {

                        echo "Old user has old information" . PHP_EOL;
                        echo $oldUser->modified . " " . $newUser->modified . PHP_EOL;
                        echo "skipped user updation" . PHP_EOL;

                    }
                } else {

                    echo "Old user does not exists. needs to be imported" . PHP_EOL;
                    // dd($newUserData);
                    $newUser = DonorNew::create($transformedData);
                    echo "Created new user with old data" . PHP_EOL;
                    
                }

                echo "==========".PHP_EOL;

            }

        });
    }


    function getTransformedUser($modelToBeTransformed) {
        // $transformedDataa = [];

        $transformedData = array_fill_keys(\Schema::getColumnListing('donors_new'), null);

        unset($transformedData['id']);
        // dd($modelToBeTransformed->toArray());

        $oldUserArray = $modelToBeTransformed->toArray();
        $oldUserProfileArray = $modelToBeTransformed->profile()->get()->toArray();
        
        unset($oldUserArray['id']);
        unset($oldUserArray['is_migrated']);
        unset($oldUserProfileArray['is_migrated']);
        
        // print_r($oldUserArray);
        if(!count($oldUserProfileArray)) {
            // echo "no profile" . PHP_EOL;
            $mergedDataToBeTransformed = $oldUserArray;
        } else {
            // echo "with profile" . PHP_EOL;
            // print_r($oldUserProfileArray);
            $organsArray = json_decode($oldUserProfileArray[0]['organs'], true);
            if(is_null($organsArray)) $organsArray = [];
            // print_r(array_merge($oldUserArray, ($oldUserProfileArray[0]), $organsArray));
            $mergedDataToBeTransformed = (array_merge($oldUserArray, ($oldUserProfileArray[0]), $organsArray));
        }

        unset($mergedDataToBeTransformed['id']);
        unset($mergedDataToBeTransformed['user_id']);
        unset($mergedDataToBeTransformed['user_old_id']);
        unset($mergedDataToBeTransformed['user_old_old_id']);


        foreach($mergedDataToBeTransformed as $key => $value) {
            if(array_key_exists($key, $transformedData)) {
                // echo "Key exists: {" . $key. " => " . $value . "}" . PHP_EOL;
                $transformedData[$key] = $value;
            }
        }
        
        if(\Carbon\Carbon::parse($transformedData['last_login'])->year < 0) {
            $transformedData['last_login'] = null;
        }

        // dd(\Carbon\Carbon::parse($mergedDataToBeTransformed['created'])->year);
        if(\Carbon\Carbon::parse($transformedData['created'])->year < 0) {
            // echo "invalid date";
            $transformedData['created'] = null;
        }

        if(\Carbon\Carbon::parse($transformedData['updated'])->year < 0) {
            // echo "invalid date";
            $transformedData['updated'] = null;
        }
        
        if(\Carbon\Carbon::parse($transformedData['harvested_on'])->year < 0) {
            $transformedData['harvested_on'] = null;
        }
        
        $transformedData['is_deleted'] = false;

        $transformedData['type'] = 'donor';

        if($transformedData['origin'] == null) { 
            $transformedData['origin'] = 'unknown';
        }
        
        if($transformedData['activated'] == null) { 
            $transformedData['activated'] = true;
        }
        
        if($transformedData['banned'] == null) { 
            $transformedData['banned'] = true;
        }
        
        if($transformedData['score'] == null) { 
            $transformedData['score'] = 0;
        }
        
        if($transformedData['is_photo_uploaded'] == null) { 
            $transformedData['is_photo_uploaded'] = 0;
        }
        
        if($transformedData['level'] == null) { 
            $transformedData['level'] = 0;
        }
        if($transformedData['is_harvested'] == null) { 
            $transformedData['is_harvested'] = 0;
        }
        

        // dd($transformedData);
        
        // dd($newUserDataArray);
        return $transformedData;
    }

    /**
     * Execute the console command.
     *
     * Imports Users taable in donorss_new taable
     * @return mixed
     */
    public function _handle() {

        \Log::info('Strating Migration from last users table');
        // User::where('is_migrated', false)->chunk(100, function($oldUsers) {
        User::chunk(100, function($oldUsers) {
            
            \Log::info('Chunked next 100 users');

            foreach($oldUsers as $oldUser) {
                \Log::info('Migrating user # '. $oldUser->id . ' <'.$oldUser->email.'>');
                // unset($oldUsers[id]);
                $oldUserArray = $oldUser->toArray();
                $oldUserProfileArray = $oldUser->profile()->get()->toArray();
                unset($oldUserArray['id']);
                
                unset($oldUserArray['is_migrated']);

                unset($oldUserProfileArray['is_migrated']);

                if(!count($oldUserProfileArray)) {
                    $newUser = $oldUserArray;
                } else {
                    $organsArray = json_decode($oldUserProfileArray[0]['organs'], true);
                    if(is_null($organsArray)) $organsArray = [];
                    // print_r(array_merge($oldUserArray, ($oldUserProfileArray[0]), $organsArray));
                    $newUser = (array_merge($oldUserArray, ($oldUserProfileArray[0]), $organsArray));
                }

                unset($newUser['id']);
                unset($newUser['user_id']);
                
                $newUser = DonorNew::create($newUser);
                // if(count($oldUserProfileArray)) {
                    // $newUser->profile()->create($oldUserProfileArray);
                // }
                \Log::info('Marking user as migrated in DB');
                \Log::info('Migrated user # '. $oldUser->id . ' <'.$oldUser->email.'>');
            }

        });
    }
}
