<?php
namespace App\Transformers;

use League\Fractal;

class PostTransformer extends Fractal\TransformerAbstract
{
	public function transform($post)
	{
		// $transformedPost = [];
		switch($post->source) {
			case 'twitter' :
				return [
					'id' => $post->id,
					'caption' => $post->text,
					'user' => [
						$post->user->id
					],
					'media' => [
						'url' => (isset($post->entities->media) ? $post->entities->media[0]->media_url_https : null)
					],
					'created_at' => \Carbon\Carbon::parse($post->created_at)->toDateTimeString(),
					'source' => $post->source
				];
				break;
			case 'instagram' :
				return [
					'id' => $post->id,
					'caption' => $post->caption->text,
					'user' => [
						$post->user->id
					],
					'media' => [
						'url' => $post->images->standard_resolution->url
					],
					'created_at' => \Carbon\Carbon::createFromTimestampUTC($post->created_time)->toDatetimeString(),
					'source' => $post->source
				];
				break;
		}
		
	}
}