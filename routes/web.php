<?php

use App\Models\User;
use App\Models\Profile;
use App\Models\Donee;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;
use Mail as Mail;
use Carbon\Carbon;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('test', 'TestController@index');

Route::get('cron', function() {

    User::where('created', '>=', Carbon::now()->subDay())->chunk(100, function($users) {
        
        foreach($users as $user) {

                if(!$user->profile) {
                    echo $user->email . PHP_EOL;


                    Mail::send('emails.reminder', [],
                        function ($message) use($user) {
                            $message->from('info@organdonationday.in');
                            $message->to($user->email);
                            $message->bcc('hello@neerajkumar.name');
                            $message->subject("You're One Step Away");
                        }
                    );
                    
                    
                    
                } else {
                    ;
                }

        }

    });
});