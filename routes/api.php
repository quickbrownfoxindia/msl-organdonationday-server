<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['Middleware' => 'json.request|jwt.auth', 'prefix'  =>  'v1', 'namespace'   =>  'API\V1'], function ()    {
    Route::post('add-user', 'UsersController@addUser')->name('add-user');
    Route::post('update-user', 'UsersController@addProfile')->name('update-user');
    Route::post('send-mail', 'ContactsController@contact')->name('send-mail');
    Route::post('show-page', 'PagesController@showPage')->name('show-page');
    Route::post('search-user', 'SearchController@searchUser')->name('search-user');
    Route::post('delete-user', 'UsersController@deleteUser')->name('delete-user');
    Route::post('update-donee', 'UsersController@addDonee')->name('update-donee');
    Route::post('delete-donee', 'UsersController@deleteDonee')->name('delete-donee');
    Route::get('get-user-details/{id}', 'UsersController@getUserDetails')->name('get-user-details');
    Route::get('get-donor-count', 'UsersController@getDonorCount')->name('get-donor-count');
    Route::get('send-reminder-email', 'CronController@sendReminderEmail')->name('send-reminder-email');
});
