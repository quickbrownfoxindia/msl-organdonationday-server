<div>
	<div style="padding:0; margin:0">
		<table border="0" cellpadding="0" cellspacing="0" width="100%" style="margin:0; padding:0">
			<tbody>
				<tr>
					<td align="center" valign="top">
						<table border="0" cellpadding="0" cellspacing="0" width="600" style="margin:0; padding:0">
							<tbody>
								<tr>
									<td align="" valign="top">
										<p style="font-family:'Roboto',arial,sans-serif; font-style:normal; font-weight:300">Dear {{ ucwords($name) }}, </p>
										<p style="font-family:'Roboto',arial,sans-serif; font-style:normal; font-weight:300">Thank you for signing up and showing intent to be an Organ Donor. A donor card will be sent to your address directly. You can also download an e-donor card <a href="https://goo.gl/5Uxfka" target="_blank" rel="noopener noreferrer">here</a>.</p>
										<p style="font-family:'Roboto',arial,sans-serif; font-style:normal; font-weight:300">We request you to sign and carry the donor card with you at all times. Please discuss your wish to donate organs with your family and friends and inform them about the same because their consent will be required at the time of donation. We also request you to visit our websites <a href="https://mandrillapp.com/track/click/30148388/www.organdonationday.in?p=eyJzIjoieHZVTGVJSzB0YUFNNFd0Q3RraTRrVWZDd0p3IiwidiI6MSwicCI6IntcInVcIjozMDE0ODM4OCxcInZcIjoxLFwidXJsXCI6XCJodHRwOlxcXC9cXFwvd3d3Lm9yZ2FuZG9uYXRpb25kYXkuaW5cIixcImlkXCI6XCJmY2QzZWMxODE5MTI0OWRjOTk3YjI4NjA5ODFkMjAyOVwiLFwidXJsX2lkc1wiOltcImU3ZDI4NzNhZWUyN2U2YjZhMDZkODZmZWFjYzA4YjJiNWM2MGJkY2VcIl19In0" target="_blank" rel="noopener noreferrer">www.organdonationday.in</a> &amp; <a href="https://mandrillapp.com/track/click/30148388/www.kdah.com?p=eyJzIjoiemloVVMycUlLN0Y1cTZNNnRwemVDYmdrcXYwIiwidiI6MSwicCI6IntcInVcIjozMDE0ODM4OCxcInZcIjoxLFwidXJsXCI6XCJodHRwOlxcXC9cXFwvd3d3LmtkYWguY29tXCIsXCJpZFwiOlwiZmNkM2VjMTgxOTEyNDlkYzk5N2IyODYwOTgxZDIwMjlcIixcInVybF9pZHNcIjpbXCJjZDIwNWIyMzIxZmQ3MjMwZTg2YzcyMGE0YWIwZTBiMWJiNzhhMTk5XCJdfSJ9" target="_blank" rel="noopener noreferrer">www.kdah.com</a> and understand the concept of 'brain-death' and when organs can be donated.</p>
										<p style="font-family:'Roboto',arial,sans-serif; font-style:normal; font-weight:300">We would like you to help us in this cause by introducing Organ Donation Drive to your family and friends and spreading the word about this critical cause. Simply forward this email or share the link below on your favorite forum, blog or social media site to invite your friends to be part of this social cause.</p>
										<p style="font-family:'Roboto',arial,sans-serif; font-style:normal; font-weight:300"><a href="https://mandrillapp.com/track/click/30148388/organdonationday.in?p=eyJzIjoiQUgwX3VMUGJGMGxsclVIM2FfYkZYZmE5LXZBIiwidiI6MSwicCI6IntcInVcIjozMDE0ODM4OCxcInZcIjoxLFwidXJsXCI6XCJodHRwOlxcXC9cXFwvb3JnYW5kb25hdGlvbmRheS5pblxcXC9cIixcImlkXCI6XCJmY2QzZWMxODE5MTI0OWRjOTk3YjI4NjA5ODFkMjAyOVwiLFwidXJsX2lkc1wiOltcImU2MTE1ZjdmNDJmNzhjODRlMTdkOWE5MmM3NzFhODM5NDE0OTk1N2NcIl19In0" target="_blank" rel="noopener noreferrer">http://organdonationday.in/</a></p>
										<p style="font-family:'Roboto',arial,sans-serif; font-style:normal; font-weight:300">Thanks &amp; Regards,<br>Kokilaben Dhirubhai Ambani Hospital and The Times of India</p>
										<table border="0" cellpadding="0" cellspacing="0" width="100%" style="margin:0; padding:0">
											<tbody>
												<tr>
													<td style="padding-right:2em;"><img src="http://old.organdonationday.in/./statics/i/organ_donation_logo.jpg" height="100px"> </td>
													<td style="padding-right:2em;"><strong>Presented By:</strong><br><br/><a href="https://mandrillapp.com/track/click/30148388/www.kokilabenhospital.com?p=eyJzIjoieFNyWWlyc1FmeGpwZVVDQWNNVE1qU19Xd3RJIiwidiI6MSwicCI6IntcInVcIjozMDE0ODM4OCxcInZcIjoxLFwidXJsXCI6XCJodHRwOlxcXC9cXFwvd3d3Lmtva2lsYWJlbmhvc3BpdGFsLmNvbVxcXC8_dXRtX3NvdXJjZT1vcmdhbl93ZWJzaXRlJnV0bV9tZWRpdW09d2Vic2l0ZSZ1dG1fdGVybT1vcmdhbmRvbmF0aW9uJnV0bV9jb250ZW50PXdlYnNpdGUmdXRtX2NhbXBhaWduPW9yZ2FuX3dlYnNpdGVcIixcImlkXCI6XCJmY2QzZWMxODE5MTI0OWRjOTk3YjI4NjA5ODFkMjAyOVwiLFwidXJsX2lkc1wiOltcImQ0OWFhMjEyNGViZjVlODJlMmFiYzlhMDM0YTAyMmY4OTBiYzdmZjlcIl19In0" target="_blank" rel="noopener noreferrer"><img src="http://old.organdonationday.in/./statics/i/KDAHLOGO.jpg" height="65px"></a></td>
													<td style="padding-right:2em;"><strong>An Intiative By:</strong><br><img src="http://organdonationday.in/assets/img/logo.png" height="80"> </td>
												</tr>
											</tbody>
										</table>
									</td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
			</tbody>
		</table>
		<img src="https://mandrillapp.com/track/open.php?u=30148388&amp;id=fcd3ec18191249dc997b2860981d2029" height="1" width="1">
	</div>
</div>