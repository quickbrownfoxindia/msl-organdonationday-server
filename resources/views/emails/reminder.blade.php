<p>Dear User,</p>
<p>We noticed you started your journey towards becoming an organ donor on our website. You are now only a step away from bringing about a change in someone else’s life. Just follow this <a href="https://goo.gl/2G8dkz">link</a>, and fill in your details.</p>
<p>Thank you,<br/>
Kokilaben Dhirubhai Ambani Hospital<br/>
and<br/>
The Times of India</p>